# Liquid Simulation

Books

Andrews 2000;
Houze 1993;
Rogers and Yau 1989


* velocity, `u=(u, v, w)`
* air pressure, `p`
* temperature, `T`
* water vapor, `qv`
* condensed cloud water, `qc`

These water content variables are mixing ratios –
the mass of vapor or liquid water per unit mass of air.

It is the condensed water, `qc`, that makes clouds visible, so this is the desired output of our simulation.

We require a system of equations that models cloud dynamics in terms of these variables. These equations are :

* equations of motion
* thermodynamic equation
* water continuity equations

## Equations of motion

The motion of air in the atmosphere can be described by the
incompressible Euler equations of fluid motion

$$

au / at = -(u \dot{} delta) u - (1 / P) * delta * p + B * k + f

$$

<br>

* density of fluid, `P`

##

* potential temperature, θ