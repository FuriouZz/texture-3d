var $main_canvas, main_ctx


module.exports = {
  index: 0,
  canvasIndex: -1,
  size: 64,
  canvasTileSize: 8,

  $canvases: [],

  initialize() {
    $main_canvas = document.createElement('canvas')
    main_ctx = $main_canvas.getContext('2d')

    $main_canvas.width  = this.size * this.canvasTileSize
    $main_canvas.height = this.size * this.canvasTileSize
    $main_canvas.style.cssText = `
    width:  ${this.size * this.canvasTileSize}px;
    height: ${this.size * this.canvasTileSize}px;
    position: absolute;
    z-index: 1000;
    background-color: red;
    opacity: 0.25;
    `

    window.DEBUG_CANVAS = $main_canvas
    document.body.appendChild( $main_canvas )
  },

  create() {
    var $canvas = document.createElement('canvas')
    var ctx = $canvas.getContext('2d')
    $canvas.width  = this.size
    $canvas.height = this.size

    this.$canvases.push({
      canvas:    $canvas,
      context:   ctx,
      imageData: ctx.createImageData(this.size, this.size)
    })

    this.canvasIndex++
    this.index = 0
  },

  set(value) {
    value = parseInt(value)
    this.$canvases[this.canvasIndex].imageData.data[this.index+0] = value
    this.$canvases[this.canvasIndex].imageData.data[this.index+1] = value
    this.$canvases[this.canvasIndex].imageData.data[this.index+2] = value
    this.$canvases[this.canvasIndex].imageData.data[this.index+3] = 255
    this.index += 4
  },

  draw() {
    for (let i = 0; i < this.canvasIndex+1; i++) {
      this.$canvases[i].context.putImageData(
        this.$canvases[i].imageData,
        0., 0.,
        0., 0.,
        this.size, this.size
      )

      main_ctx.drawImage(
        this.$canvases[i].canvas,
        (i % this.canvasTileSize) * this.size,
        parseInt(i / this.canvasTileSize) * this.size
      )
    }
  }
}